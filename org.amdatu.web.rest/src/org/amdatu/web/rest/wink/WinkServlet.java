/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.wink;

import static java.security.AccessController.doPrivileged;
import static org.amdatu.web.rest.jaxrs.JaxRsRequestInterceptor.REQUEST_INTERCEPTOR_ID;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_SELECT;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_SERVLET_PATTERN;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.atomic.AtomicReference;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Path;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import org.amdatu.web.rest.jaxrs.InterceptRequest;
import org.amdatu.web.rest.jaxrs.JaxRsRequestInterceptor;
import org.amdatu.web.rest.jaxrs.RequestContext;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.apache.wink.server.handlers.AbstractHandler;
import org.apache.wink.server.handlers.MessageContext;
import org.apache.wink.server.handlers.RequestHandler;
import org.apache.wink.server.internal.DeploymentConfiguration;
import org.apache.wink.server.internal.RequestProcessor;
import org.apache.wink.server.internal.handlers.SearchResult;
import org.apache.wink.server.internal.registry.ResourceRegistry;
import org.apache.wink.server.utils.RegistrationUtils.InnerApplication;
import org.osgi.framework.ServiceReference;
import org.osgi.service.http.context.ServletContextHelper;
import org.osgi.service.log.LogService;

/**
 * Servlet for handling JAX-RS requests.
 */
public class WinkServlet extends HttpServlet {
    
    private static final long serialVersionUID = 1L;
    
    private volatile WinkServletConfig m_config;
    private volatile Component m_component;
    private volatile LogService m_log;
    
    private final ConcurrentMap<ServiceReference<Object>, Object> m_jaxRsResources;
    private final ConcurrentNavigableMap<ServiceReference<Object>, Application> m_jaxRsApps;
    private final AtomicReference<RequestProcessor> m_requestProcessorRef;
    private final WinkRequestInterceptorHandler m_requestInterceptorHandler;
    
    public WinkServlet() {
        m_config = new WinkServletConfig();
        m_requestProcessorRef = new AtomicReference<>();
        m_jaxRsResources = new ConcurrentHashMap<>();
        m_jaxRsApps = new ConcurrentSkipListMap<>();
        m_requestInterceptorHandler = new WinkRequestInterceptorHandler();
    }
    
    @Override
    public void service(final ServletRequest req, final ServletResponse resp) throws ServletException, IOException {
        try {
            // See WINK-427; basically we're not sure whether the HTTP server is running with the
            // same set of privileges as the Amdatu-Web bundle, so we need to be a little more
            // careful when making calls from one to the other...
            doPrivileged(new PrivilegedExceptionAction<Void>() {
                @Override
                public Void run() throws IOException, ServletException {
                    // It could be that the request is not for us, for example, because of a request
                    // to another non-REST servlet. We ask the manager if it is able to handle this
                    // request, and if not, simply chain it to the next filter or servlet...
                    if (req instanceof HttpServletRequest && resp instanceof HttpServletResponse) {
                        HttpServletRequest httpServletRequest = (HttpServletRequest) req;
                        HttpServletResponse httpServletResponse = (HttpServletResponse) resp;
                        if (!handleRequest(httpServletRequest, httpServletResponse)) {
                            httpServletResponse.sendError(HttpServletResponse.SC_NOT_FOUND);
                        } 
                    }
                    return null;
                }
            });
        } catch (PrivilegedActionException exception) {
            Exception wrapped = exception.getException();
            if (wrapped instanceof IOException) {
                throw (IOException) wrapped;
            } else if (wrapped instanceof ServletException) {
                throw (ServletException) wrapped;
            } else {
                throw new RuntimeException("Unexpected exception in WinkServlet!", wrapped);
            }
        }
    }
    
    protected final void added(ServiceReference<ServletContextHelper> ref) {
        updateConfig(new WinkServletConfig(ref));
    }
    
    protected final void changed(ServiceReference<ServletContextHelper> ref) {
        updateConfig(new WinkServletConfig(ref));  
    }
    
    protected final void removed(ServiceReference<ServletContextHelper> ref) {
        m_jaxRsApps.clear();
        m_jaxRsResources.clear();
    }
    
    protected final void dmInit() {
        DependencyManager dm = m_component.getDependencyManager();
        updateServletProperties(m_config);
        
        String filter = m_config.getServletContextFilter();
        m_component.add(
            dm.createServiceDependency()
                .setService(String.format("(&(objectClass=*)%s)", filter))
                .setCallbacks("onAdded", "onRemoved"),
            dm.createServiceDependency()
                .setService(JaxRsRequestInterceptor.class, filter)
                .setRequired(false)
                .setCallbacks(m_requestInterceptorHandler, "addedInterceptor", "removedInterceptor")
            );
    }
    
    /**
     * Called by Felix DM for each service registration.
     */
    protected final void onAdded(ServiceReference<Object> serviceReference, Object service) {
        Class<? extends Object> type = service.getClass();

        if (Application.class.isAssignableFrom(type)) {
            Application app = (Application) service;
            if (m_jaxRsApps.putIfAbsent(serviceReference, app) == null) {
                // Need to add it ourselves...
                markRequestProcessorOutdated();
            }
        } else if (type.isAnnotationPresent(Path.class) || type.isAnnotationPresent(Provider.class)) {
            if (m_jaxRsResources.putIfAbsent(serviceReference, service) == null) {
                // Need to add it ourselves...
                markRequestProcessorOutdated();
            }
        }
    }

    /**
     * Called by Felix DM for each service deregistration.
     */
    protected final void onRemoved(ServiceReference<Object> serviceReference, Object service) {
        Class<? extends Object> type = service.getClass();
        if (Application.class.isAssignableFrom(type)) {
            Application app = (Application) service;
            if (m_jaxRsApps.remove(serviceReference, app)) {
                // Since it seems not possible to remove from RequestProcessor, we need to create a new one...
                markRequestProcessorOutdated();
            }
        } else if (type.isAnnotationPresent(Path.class) || type.isAnnotationPresent(Provider.class)) {
            if (m_jaxRsResources.remove(serviceReference, service)) {
                // Since it seems not possible to remove from RequestProcessor, we need to create a new one...
                markRequestProcessorOutdated();
            }
        }
    }
    
    /**
     * Delegates the given HTTP request to Wink to be handled as JAX-RS request if it is a valid/known JAX-RS resource.
     * 
     * @param req the HTTP servlet request, cannot be <code>null</code>;
     * @param resp the HTTP servlet response, cannot be <code>null</code>.
     * @return <code>true</code> if the given request is handled by Wink as JAX-RS request, <code>false</code> otherwise.
     * @throws ServletException in case of exceptions during the handling of the JAX-RS request.
     */
    public boolean handleRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
        // Wrap the request as we need to remove the configured prefix from the request URL...
        PrefixedHttpServletRequest request = new PrefixedHttpServletRequest(req);

        // Will initialize one if needed, do *not* use the corresponding field directly!
        RequestProcessor reqProc = getRequestProcessor();

        DeploymentConfiguration deploymentConfiguration = reqProc.getConfiguration();
        ResourceRegistry resourceRegistry = deploymentConfiguration.getResourceRegistry();
        // We only need to know if at least one resource could match the request URI...
        List<?> rootResources = resourceRegistry.getMatchingRootResources(request.getRequestURI(), false);

        boolean canHandle = !rootResources.isEmpty();
        if (canHandle) {
            reqProc.handleRequest(request, resp);
        }
        return canHandle;
    }
    
    private RequestProcessor createRequestProcessor() {
        WinkServletConfig config = m_config;

        DeploymentConfiguration configuration = new DeploymentConfiguration() {
            @Override
            protected List<RequestHandler> initRequestUserHandlers() {
                List<RequestHandler> requestHandlers = new ArrayList<>();
                // AMDATUWEB-51: Pass the original HttpServletRequest to resources that inject the HttpServletRequest
                requestHandlers.add(new UnwrapPrefixedHttpServletRequestHandler());
                // Add a RequestHandler that handles @InterceptRequest annotations and calls JaxRsRequestInterceptors
                requestHandlers.add(m_requestInterceptorHandler);
                return requestHandlers;
            }
        };
        configuration.setProperties(config.getWinkProperties());
        
        configuration.init();
        
        // Take the application with the highest ranking...
        Entry<ServiceReference<Object>, Application> lastEntry = m_jaxRsApps.lastEntry();
        if (lastEntry != null) {
            configuration.addApplication(lastEntry.getValue(), false);
        } else {
            configuration.addApplication(new WinkApplication(m_log), false);
        }

        // Put all locally managed resources in a separate application...
        Object[] resources = m_jaxRsResources.values().toArray();
        configuration.addApplication(new InnerApplication(resources), false);

        return new RequestProcessor(configuration);
    }

    /**
     * Used in JaxRsFilter to check if the path can be handled by wink
     */
    private RequestProcessor getRequestProcessor() {
        synchronized (m_requestProcessorRef) {
            RequestProcessor rp = m_requestProcessorRef.get();
            if (rp == null) {
                RequestProcessor newRP = createRequestProcessor();
                do {
                    rp = m_requestProcessorRef.get();
                } while (!m_requestProcessorRef.compareAndSet(rp, newRP));
            }
            return m_requestProcessorRef.get();
        }
    }

    public void updateConfig(WinkServletConfig newConfig) {
        if (!newConfig.equals(m_config)){
            m_config = newConfig;
            markRequestProcessorOutdated();
            updateServletProperties(newConfig);
        }
    }
    
    void markRequestProcessorOutdated() {
        synchronized (m_requestProcessorRef) {
            m_requestProcessorRef.set(null);
        }
    }
    
    private void updateServletProperties(WinkServletConfig config) {
        Properties props = new Properties();
        props.put(HTTP_WHITEBOARD_SERVLET_PATTERN, config.getServletPattern());
        props.put(HTTP_WHITEBOARD_CONTEXT_SELECT, config.getContextSelectFilter());
        m_component.setServiceProperties(props);
    }
    
    /**
     * Servlet request wrapper needed for removing the configured prefix from our requests,
     * in order for Wink to properly resolve the request to a resource.
     */
    static class PrefixedHttpServletRequest extends HttpServletRequestWrapper {
        
        private final HttpServletRequest m_request; 
        
        public PrefixedHttpServletRequest(HttpServletRequest request) {
            super(request);
            m_request = request;
        }
        
        public HttpServletRequest unwrap() {
            return m_request;
        }

        @Override
        public String getRequestURI() {
            String requestURI = super.getRequestURI();
            requestURI = requestURI.substring(super.getContextPath().length()); // Remove context path
            requestURI = requestURI.substring(super.getServletPath().length()); // Remove servlet path
            return requestURI;
        }
        
        @Override
        public String getServletPath() {
            // Wink doesn't know it's on another path than root
            return "";
        }
        @Override
        public String getContextPath() {
            // Wink doesn't know it's on another path than root
            return "";
        }
    }
    
    /**
     * Handler that unwraps the {@link PrefixedHttpServletRequest} so resources that inject the {@link HttpServletRequest} 
     * get the original request injected
     */
    public static class UnwrapPrefixedHttpServletRequestHandler extends AbstractHandler {

        @Override
        protected void handleRequest(MessageContext context) throws Throwable {
            SearchResult searchResult = context.getAttribute(SearchResult.class);
            Object[] parameters = searchResult.getInvocationParameters();

            for (int i = 0; i < parameters.length; i++) {
                Object p = parameters[i];
                if (p instanceof PrefixedHttpServletRequest) {
                    parameters[i] = ((PrefixedHttpServletRequest) p).unwrap();
                }
            }
        }
    }
    
    /**
     * Wink Handler implementation that calls {@link JaxRsRequestInterceptor} when the class or method is annotated with a
     * {@link InterceptRequest} annotation.
     */
    public static class WinkRequestInterceptorHandler extends AbstractHandler {

        private final NavigableMap<ServiceReference<JaxRsRequestInterceptor>, JaxRsRequestInterceptor> m_interceptors;

        public WinkRequestInterceptorHandler() {
            m_interceptors = new ConcurrentSkipListMap<>(Collections.reverseOrder());
        }

        protected final void addedInterceptor(ServiceReference<JaxRsRequestInterceptor> ref,
            JaxRsRequestInterceptor interceptor) {
            m_interceptors.put(ref, interceptor);
        }

        protected final void removedInterceptor(ServiceReference<JaxRsRequestInterceptor> ref) {
            m_interceptors.remove(ref);
        }

        @Override
        protected void handleRequest(MessageContext context) throws Throwable {
            SearchResult searchResult = context.getAttribute(SearchResult.class);
            Object instance = searchResult.getResource().getInstance(context);

            Method javaMethod = searchResult.getMethod().getMetadata().getReflectionMethod();
            Object[] parameters = searchResult.getInvocationParameters();

            Set<String> interceptorIds = new HashSet<>();
            getInterceptorIds(interceptorIds, instance.getClass().getAnnotations());
            getInterceptorIds(interceptorIds, javaMethod.getAnnotations());

            if (interceptorIds.isEmpty()) {
                return; // Return early no need to do anything
            }

            HttpServletRequest request = context.getAttribute(HttpServletRequest.class);
            HttpServletResponse response = context.getAttribute(HttpServletResponse.class);
            RequestContext requestContext = new RequestContext(instance, javaMethod, parameters, request, response);

            m_interceptors.forEach((ref, interceptor) -> {
                if (interceptorIds.remove(ref.getProperty(REQUEST_INTERCEPTOR_ID))) {
                    interceptor.intercept(requestContext);
                }
            });

            if (!interceptorIds.isEmpty()) {
                throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
            }
        }

        private void getInterceptorIds(Set<String> interceptorIds, Annotation[] annotations) {
            for (Annotation a : annotations) {
                if (a.annotationType().isAssignableFrom(InterceptRequest.class)) {
                    String interceptorId = ((InterceptRequest) a).id();
                    interceptorIds.add(interceptorId);
                } else {
                    getInterceptorIds(interceptorIds, a.annotationType().getAnnotationsByType(InterceptRequest.class));
                }
            }
        }
    }
    
}
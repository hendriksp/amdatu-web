/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.wink;

import static java.lang.String.format;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_NAME;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_DEFAULT_CONTEXT_NAME;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Properties;

import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.ConfigurationException;

/**
 * Configuration properties for {@link WinkServlet}.
 */
public class WinkServletConfig {

    //"(|(!(osgi.http.whiteboard.context.name=*))(osgi.http.whiteboard.context.name=default))"
    private static final String CONTEXT_FILTER_DEFAULT = format("(|(!(%s=*))(%s=%s))", HTTP_WHITEBOARD_CONTEXT_NAME, HTTP_WHITEBOARD_CONTEXT_NAME, HTTP_WHITEBOARD_DEFAULT_CONTEXT_NAME);
    
    static final String CONFIG_REST_PATH = "wink.rest.path";

    /**
     * Wink prefixes all of its properties with 'wink.', while this collides with our own prefix
     * (wink.rest) it does not matter much, as Wink does not touch these.
     */
    private static final String WINK_PROPS_PREFIX = "wink.";
    
    /**
     * Prefix that should be used when configuring additional contexts using service properties
     */
    static final String SERVICE_REF_PROPS_PREFIX = "org.amdatu.web.";

    /**
     * Catch all requests by default, assume all REST resources start from the root.
     */
    private static final String DEFAULT_PATH_PREFIX = "";

    private static final Properties DEFAULT_PROPERTIES = new Properties();
    

    static {
        // Needed for supporting the issue discussed on <https://cwiki.apache.org/confluence/display/WINK/5.3+Resource+Matching>
        DEFAULT_PROPERTIES.setProperty("wink.searchPolicyContinuedSearch", "true");
        DEFAULT_PROPERTIES.setProperty("wink.rootResource", "none");
        DEFAULT_PROPERTIES.setProperty("wink.loadApplications", "false");
    }

    private final String m_pathPrefix;
    private final Properties m_winkProps;
    private final String m_contextName;

    public WinkServletConfig() {
        m_pathPrefix = DEFAULT_PATH_PREFIX;
        m_winkProps = DEFAULT_PROPERTIES;
        m_contextName = HTTP_WHITEBOARD_DEFAULT_CONTEXT_NAME;
    }
    
    public WinkServletConfig(ServiceReference<?> ref) {
        m_contextName = (String) ref.getProperty(HTTP_WHITEBOARD_CONTEXT_NAME);

        Object restPathValue = ref.getProperty(SERVICE_REF_PROPS_PREFIX + CONFIG_REST_PATH);
        m_pathPrefix = toPathPrefix(restPathValue);
        
        m_winkProps = new Properties(DEFAULT_PROPERTIES);

        // Copy all properties starting with `wink.` and propagate them to Wink itself...
        for (String key : ref.getPropertyKeys()){
            if (key.startsWith(SERVICE_REF_PROPS_PREFIX + WINK_PROPS_PREFIX) && !(SERVICE_REF_PROPS_PREFIX + CONFIG_REST_PATH).equals(key)) {
                Object value = ref.getProperty(key);
                if (value != null) {
                    m_winkProps.setProperty(key.substring(SERVICE_REF_PROPS_PREFIX.length()), value.toString());
                }
            }
        }
    }
    
    public WinkServletConfig(Dictionary<String, ?> properties) throws ConfigurationException {
        m_contextName = HTTP_WHITEBOARD_DEFAULT_CONTEXT_NAME;
        if (properties == null) {
            m_pathPrefix = DEFAULT_PATH_PREFIX;
            m_winkProps = DEFAULT_PROPERTIES;
        } else {
            Object value = properties.get(CONFIG_REST_PATH);
            String pathPrefix;
            try {
                pathPrefix = toPathPrefix(value);
            } catch (IllegalArgumentException e) {
                throw new ConfigurationException(CONFIG_REST_PATH, e.getMessage());
            }

            m_pathPrefix = pathPrefix;

            m_winkProps = new Properties(DEFAULT_PROPERTIES);

            // Copy all properties starting with `wink.` and propagate them to Wink itself...
            Enumeration<String> keys = properties.keys();
            while (keys.hasMoreElements()) {
                String key = keys.nextElement();
                if (key.startsWith(WINK_PROPS_PREFIX) && !CONFIG_REST_PATH.equals(key)) {
                    value = properties.get(key);
                    if (value != null) {
                        m_winkProps.setProperty(key, value.toString());
                    }
                }
            }
        }
    }
    
    private static String toPathPrefix(Object value) {
        String pathPrefix = DEFAULT_PATH_PREFIX;
        if (value != null && !(value instanceof String)) {
            throw new IllegalArgumentException("not a string value");
        } else if (value != null) {
            pathPrefix = ((String) value).trim();
        }

        if (!"".equals(pathPrefix) && !"/".equals(pathPrefix)
            && (!pathPrefix.startsWith("/") || pathPrefix.endsWith("/"))) {
            throw new IllegalArgumentException("value should start with '/' and not end with '/'!");
        }
        return pathPrefix;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        WinkServletConfig other = (WinkServletConfig) obj;
        if (!m_pathPrefix.equals(other.m_pathPrefix)) {
            return false;
        }
        if (!m_winkProps.equals(other.m_winkProps)) {
            return false;
        }

        return true;
    }


    /**
     * @return the configured path prefix for all requests that are to be handled by Wink, cannot be <code>null</code>.
     */
    public String getPathPrefix() {
        return m_pathPrefix;
    }

    /**
     * @return a servlet pattern regex for registering the {@link WinkServlet}.
     */
    public String getServletPattern() {
        return m_pathPrefix.concat("/*");
    }

    /**
     * @return the Wink-specific properties, never <code>null</code>.
     */
    public Properties getWinkProperties() {
        return m_winkProps;
    }

    @Override
    public int hashCode() {
        final int prime = 37;
        int result = 1;
        result = prime * result + m_pathPrefix.hashCode();
        result = prime * result + m_winkProps.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "[path prefix = " + m_pathPrefix + ", wink properties = " + m_winkProps + "]";
    }

    public String getContextSelectFilter() {
        return String.format("(%s=%s)", HTTP_WHITEBOARD_CONTEXT_NAME, m_contextName);
    }

    public String getServletContextFilter() {
        String filter; 
        if (HTTP_WHITEBOARD_DEFAULT_CONTEXT_NAME.equals(m_contextName)) {
            filter = CONTEXT_FILTER_DEFAULT;
        } else {
            filter = format("(%s=%s)", HTTP_WHITEBOARD_CONTEXT_NAME, m_contextName);
        }
        return filter;
    }

}

/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.wink;

import static java.lang.String.format;
import static org.amdatu.web.rest.wink.WinkServletConfig.CONFIG_REST_PATH;
import static org.amdatu.web.rest.wink.WinkServletConfig.SERVICE_REF_PROPS_PREFIX;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_NAME;

import java.util.Dictionary;
import java.util.Properties;

import javax.servlet.Servlet;
import javax.ws.rs.ext.RuntimeDelegate;

import org.amdatu.web.rest.jaxrs.JaxRsSpi;
import org.amdatu.web.rest.wink.util.DummyJaxRsSpi;
import org.amdatu.web.rest.wink.util.DummyRuntimeDelegate;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.apache.wink.common.internal.runtime.RuntimeDelegateImpl;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.http.context.ServletContextHelper;
import org.osgi.service.log.LogService;
import org.slf4j.impl.StaticLoggerBinder;

/**
 * This is the OSGi activator for the Amdatu REST framework based on Apache Wink.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class Activator extends DependencyActivatorBase {
    
	/** PID by which the registered service can be configured. */
    public static final String CONFIG_PID = "org.amdatu.web.rest.wink";
    
    /** Configuraiton property key that can be used to disable the WinkServlet for the default ServletContext */
    private static final String CONFIG_DEFAULT_CONTEXT_DISABLED = "default.context.disabled";

    private static final String CONTEXT_FILTER_WINK = format("(&(%s=*)(%s=*))", SERVICE_REF_PROPS_PREFIX + CONFIG_REST_PATH, HTTP_WHITEBOARD_CONTEXT_NAME);
    
    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
        setRuntimeDelegate();
        
        manager.add(createComponent() 
            .setInterface(JaxRsSpi.class.getName(), null) 
            .setImplementation(DummyJaxRsSpi.class)
        );
        
    	manager.add(createComponent()
			.setImplementation(StaticLoggerBinder.getSingleton())
			.add(createServiceDependency()
				.setService(LogService.class)
				.setRequired(false)
			)
		);
    	
    	Properties props = new Properties();
    	props.put(Constants.SERVICE_PID, CONFIG_PID);
    	manager.add(createComponent()
    	    .setInterface(ManagedService.class.getName(), props)
    	    .setImplementation(new DefaultContextServletManager())
    	    );
    	
    	manager.add(createAdapterService(ServletContextHelper.class, CONTEXT_FILTER_WINK, "added", "changed", "removed")
            .setInterface(Servlet.class.getName(), null)
            .setImplementation(WinkServlet.class)
            .setCallbacks("dmInit", "dmStart", "dmStop", "dmDestroy")
            .add(createServiceDependency().setService(LogService.class).setRequired(false))
            );
    	
    }
    
    /**
     * Sets the runtime delegate, used to create instances of desired endpoint classes.
     */
    private void setRuntimeDelegate() {
        /*
         * OK, this is nasty, but necessary. Without this piece of code a NoClassDefFoundError is thrown
         * during servlet initialization on the class com.sun.ws.rs.ext.RuntimeDelegateImpl. The reason
         * for this is that JAX-RS by default delegates to this class if no other RuntimeDelegate
         * implementation could be found. Apache Wink does come with its own RuntimeDelegate, but during
         * initialization of this instance, RuntimeDelegate.getInstance() is invoked which causes a method
         * call to com.sun.ws.rs.ext.RuntimeDelegateImpl (see EntityTagMatchHeaderDelegate).
         * Other frameworks face similar issues using JAX-RS, see for example this URL for the exact same
         * problem in the Restlet framework: http://www.mail-archive.com/discuss@restlet.tigris.org/msg07539.html
         * The nasty fix is to set some dummy RuntimeDelegate first, then set the Wink RuntimeDelegateImpl
         */
        RuntimeDelegate.setInstance(new DummyRuntimeDelegate());
        RuntimeDelegate.setInstance(new RuntimeDelegateImpl());
    }
    
    /**
     * Component to manage the {@link WinkServlet} for the default ServletContext to allow this instance to be configured 
     * using Configuration Admin instead of service properties on the {@link ServletContextHelper} registration.
     *
     */
    public static class DefaultContextServletManager implements ManagedService {

        private volatile DependencyManager m_dm;
        
        private volatile WinkServlet m_servlet;
        private volatile Component m_servletComp;
        
        /**
         * Called by Felix DM when the component is started
         */
        protected final void start() {
            registerWinkServlet();
        }

        /**
         * Called by Felix DM when the component is stopped
         */
        protected final void stop() {
            unregisterWinkServlet();
        }

        private void registerWinkServlet() {
            m_servlet = new WinkServlet();
            m_servletComp = m_dm.createComponent()
                            .setInterface(Servlet.class.getName(), null)
                            .setCallbacks("dmInit", "dmStart", "dmStop", "dmDestroy")
                            .setImplementation(m_servlet)
                            .add(m_dm.createServiceDependency().setService(LogService.class).setRequired(false));
            m_dm.add(m_servletComp);
        }

        private void unregisterWinkServlet() {
            if (m_servletComp != null) {
                m_dm.remove(m_servletComp);
                m_servlet = null;
                m_servletComp = null;
            }
        }
        
        @Override
        public void updated(Dictionary<String, ?> properties) throws ConfigurationException {
            
            Object disableDefaultContext = properties != null ? properties.get(CONFIG_DEFAULT_CONTEXT_DISABLED) : null;
            if (disableDefaultContext != null && Boolean.valueOf(disableDefaultContext.toString())) {
                unregisterWinkServlet();
            } else {
                if (m_servletComp == null){
                    registerWinkServlet();
                }
            }
            
            if (m_servlet != null) {
                WinkServletConfig newConfig = new WinkServletConfig(properties);
                m_servlet.updateConfig(newConfig);
            }
        }
        
    }
          
}

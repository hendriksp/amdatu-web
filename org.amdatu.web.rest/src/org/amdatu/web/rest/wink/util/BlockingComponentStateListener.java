/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.wink.util;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.apache.felix.dm.Component;
import org.apache.felix.dm.ComponentState;
import org.apache.felix.dm.ComponentStateListener;

/**
 * Provides a {@link ComponentStateListener} implementation that blocks until a certain component
 * state is reached.
 * <p>
 * The intended use for this class is to wait for a certain component state change in one thread
 * while the dependency manager changes the component state in another thread.<br>
 * This class is thread-safe.
 * </p>
 */
public class BlockingComponentStateListener implements ComponentStateListener {
    private final ComponentState m_desiredState;
    private final CountDownLatch m_latch;

    public BlockingComponentStateListener(ComponentState desiredState) {
        m_desiredState = desiredState;
        m_latch = new CountDownLatch(1);
    }

    public boolean await(long duration, TimeUnit unit) throws InterruptedException {
        return m_latch.await(duration, unit);
    }

    @Override
    public void changed(Component component, ComponentState newState) {
        if (m_desiredState.equals(newState)) {
            m_latch.countDown();
        }
    }
}
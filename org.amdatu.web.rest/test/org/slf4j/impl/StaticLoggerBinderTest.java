/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.slf4j.impl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.lang.reflect.Field;

import org.osgi.service.log.LogService;

import junit.framework.TestCase;

public final class StaticLoggerBinderTest extends TestCase {

	private final StaticLoggerBinder logger = StaticLoggerBinder.getSingleton();
	private final LogService logService = mock(LogService.class);
	private LogService originalLogService;

	@Override
	public void setUp() throws Exception {
		originalLogService = getLogService(logger);
		setLogService(logger, logService);
	}

	@Override
	public void tearDown() throws Exception {
		setLogService(logger, originalLogService);
	}

	public void testDebugWithOneArgShouldFormatMessage() {
		logger.debug("arg1: {}", 1);

		assertLogServiceContainsEntry(LogService.LOG_DEBUG, "arg1: 1");
	}

	public void testDebugWithTwoArgsShouldFormatMessage() {
		logger.debug("arg1: {}, arg2: {}", 1, 2);

		assertLogServiceContainsEntry(LogService.LOG_DEBUG, "arg1: 1, arg2: 2");
	}

	public void testDebugWithArgArrayShouldFormatMessage() {
		logger.debug("arg1: {}, arg2: {}, arg3: {}", new Object[] { 1, 2, 3 });

		assertLogServiceContainsEntry(LogService.LOG_DEBUG, "arg1: 1, arg2: 2, arg3: 3");
	}

	public void testErrorWithOneArgShouldFormatMessage() {
		logger.error("arg1: {}", 1);

		assertLogServiceContainsEntry(LogService.LOG_ERROR, "arg1: 1");
	}

	public void testErrorWithTwoArgsShouldFormatMessage() {
		logger.error("arg1: {}, arg2: {}", 1, 2);

		assertLogServiceContainsEntry(LogService.LOG_ERROR, "arg1: 1, arg2: 2");
	}

	public void testErrorWithArgArrayShouldFormatMessage() {
		logger.error("arg1: {}, arg2: {}, arg3: {}", new Object[] { 1, 2, 3 });

		assertLogServiceContainsEntry(LogService.LOG_ERROR, "arg1: 1, arg2: 2, arg3: 3");
	}

	public void testInfoWithOneArgShouldFormatMessage() {
		logger.info("arg1: {}", 1);

		assertLogServiceContainsEntry(LogService.LOG_INFO, "arg1: 1");
	}

	public void testInfoWithTwoArgsShouldFormatMessage() {
		logger.info("arg1: {}, arg2: {}", 1, 2);

		assertLogServiceContainsEntry(LogService.LOG_INFO, "arg1: 1, arg2: 2");
	}

	public void testInfoWithArgArrayShouldFormatMessage() {
		logger.info("arg1: {}, arg2: {}, arg3: {}", new Object[] { 1, 2, 3 });

		assertLogServiceContainsEntry(LogService.LOG_INFO, "arg1: 1, arg2: 2, arg3: 3");
	}

	public void testWarnWithOneArgShouldFormatMessage() {
		logger.warn("arg1: {}", 1);

		assertLogServiceContainsEntry(LogService.LOG_WARNING, "arg1: 1");
	}

	public void testWarnWithTwoArgsShouldFormatMessage() {
		logger.warn("arg1: {}, arg2: {}", 1, 2);

		assertLogServiceContainsEntry(LogService.LOG_WARNING, "arg1: 1, arg2: 2");
	}

	public void testWarnWithArgArrayShouldFormatMessage() {
		logger.warn("arg1: {}, arg2: {}, arg3: {}", new Object[] { 1, 2, 3 });

		assertLogServiceContainsEntry(LogService.LOG_WARNING, "arg1: 1, arg2: 2, arg3: 3");
	}

	private void assertLogServiceContainsEntry(final int level, final String message) {
		verify(logService).log(level, message, null);
	}

	private static Field getLogServiceField(final StaticLoggerBinder logger) throws Exception {
		final Field logServiceField = logger.getClass().getDeclaredField("m_log");
		logServiceField.setAccessible(true);
		return logServiceField;
	}

	private static LogService getLogService(final StaticLoggerBinder logger) throws Exception {
		final Field logServiceField = getLogServiceField(logger);
		return (LogService) logServiceField.get(logger);
	}

	private static void setLogService(final StaticLoggerBinder logger, final LogService logService) throws Exception {
		final Field logServiceField = getLogServiceField(logger);
		logServiceField.set(logger, logService);
	}

}

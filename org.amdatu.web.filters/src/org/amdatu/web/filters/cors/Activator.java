/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.filters.cors;

import static org.osgi.framework.Constants.SERVICE_PID;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_FILTER_REGEX;

import java.util.Properties;

import javax.servlet.Filter;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.cm.ManagedService;

public class Activator extends DependencyActivatorBase {
    
    private static final String CONFIG_PID = "org.amdatu.web.filters.cors";

    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
        Properties properties = new Properties();
        properties.put(HTTP_WHITEBOARD_FILTER_REGEX, CorsFilter.DEFAULT_PATTERN);
        properties.put(SERVICE_PID, CONFIG_PID);
        manager.add(createComponent()
            .setInterface(new String[] {Filter.class.getName(), ManagedService.class.getName()}, properties)
            .setImplementation(CorsFilter.class)
            );
    }

}

/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.filters.cors;

import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_FILTER_REGEX;

import java.io.IOException;
import java.util.Dictionary;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.dm.Component;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;

public class CorsFilter implements Filter, ManagedService {

    static final String DEFAULT_PATTERN = ".*";
    private static final String CONFIG_ALLOW_CREDENTIALS = "allowCredentials";
    private static final String CONFIG_ALLOW_HEADERS = "allowHeaders";
    private static final String CONFIG_ALLOW_METHODS = "allowMethods";
    private static final String CONFIG_ALLOW_ORIGIN = "allowOrigin";
    private static final String CONFIG_EXPOSE_HEADERS = "exposeHeaders";
    private static final String CONFIG_PATTERN = "pattern";

    private static final String DEFAULT_ALLOW_CREDENTIALS = "true";
    private static final String DEFAULT_ALLOW_HEADERS = "Origin, Content-Type, If-Match";
    private static final String DEFAULT_ALLOW_METHODS = "GET, POST, PUT, DELETE, OPTIONS, HEAD";
    private static final String DEFAULT_ALLOW_ORIGIN = "*";
    private static final String DEFAULT_EXPOSE_HEADERS = "Link, Location, ETag";

    private volatile String m_allowCredentials;
    private volatile String m_allowHeaders;
    private volatile String m_allowMethods;
    private volatile String m_allowOrigin;
    private volatile String m_exposeHeaders;

    private volatile Component m_component; 
    
    public CorsFilter() {
        setDefaults();
    }

    @Override
    public void destroy() {
        // do nothing 
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
        throws IOException, ServletException {

        HttpServletResponse response = (HttpServletResponse) res;

        response.setHeader("Access-Control-Allow-Credentials", m_allowCredentials);
        response.addHeader("Access-Control-Allow-Headers", m_allowHeaders);
        response.setHeader("Access-Control-Allow-Methods", m_allowMethods);
        response.addHeader("Access-Control-Allow-Origin", m_allowOrigin);
        response.addHeader("Access-Control-Expose-Headers", m_exposeHeaders);

        chain.doFilter(req, response);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // do nothing 
    }

    @Override
    public void updated(Dictionary<String, ?> properties) throws ConfigurationException {
        if (properties != null) {
            String allowCredentials = (String) properties.get(CONFIG_ALLOW_CREDENTIALS);
            m_allowCredentials = allowCredentials != null ? allowCredentials : DEFAULT_ALLOW_CREDENTIALS;

            String allowHeaders = (String) properties.get(CONFIG_ALLOW_HEADERS);
            m_allowHeaders = allowHeaders != null ? allowHeaders : DEFAULT_ALLOW_HEADERS;

            String allowMethods = (String) properties.get(CONFIG_ALLOW_METHODS);
            m_allowMethods = allowMethods != null ? allowMethods : DEFAULT_ALLOW_METHODS;

            String allowOrigin = (String) properties.get(CONFIG_ALLOW_ORIGIN);
            m_allowOrigin = allowOrigin != null ? allowOrigin : DEFAULT_ALLOW_ORIGIN;

            String exposeHeaders = (String) properties.get(CONFIG_EXPOSE_HEADERS);
            m_exposeHeaders = exposeHeaders != null ? exposeHeaders : DEFAULT_EXPOSE_HEADERS;

            String pattern = (String) properties.get(CONFIG_PATTERN);
            if (pattern != null) {
                updatePatternIfChanged(pattern);
            }else{
                updatePatternIfChanged(DEFAULT_PATTERN);
            }
        } else {
            setDefaults();
            updatePatternIfChanged(DEFAULT_PATTERN);
        }
    }

    private void updatePatternIfChanged(String pattern) {
        Dictionary<Object,Object> serviceProperties = m_component.getServiceProperties();
        String currentPattern = (String) serviceProperties.get(HTTP_WHITEBOARD_FILTER_REGEX);
        if (!currentPattern.equals(pattern)){
            serviceProperties.put(HTTP_WHITEBOARD_FILTER_REGEX, pattern);
            m_component.setServiceProperties(serviceProperties);
        }
    }

    private void setDefaults() {
        m_allowCredentials = DEFAULT_ALLOW_CREDENTIALS;
        m_allowHeaders = DEFAULT_ALLOW_HEADERS;
        m_allowMethods = DEFAULT_ALLOW_METHODS;
        m_allowOrigin = DEFAULT_ALLOW_ORIGIN;
        m_exposeHeaders = DEFAULT_EXPOSE_HEADERS;
    }

}

/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest;

import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_NAME;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_PATH;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.http.context.ServletContextHelper;

public class HttpWhiteBoardContextTest extends RegistrationTestBase {

    private static final String AMDATU_WEB_WINK_REST_PATH = "org.amdatu.web.wink.rest.path";
    private static final String REST_RESOURCE = "test";
    private static final String PREFIX = "/rest";
    private static final String DEFAULT_RESPONSE = "test-response";

    private static final String CONTEXT_1_NAME = "org.amdatu.web.rest.test.ctx1";
    private static final String CONTEXT_1_PREFIX = "/test1";

    private static final String CONTEXT_2_NAME = "org.amdatu.web.rest.test.ctx2";
    private static final String CONTEXT_2_PREFIX = "/test2";

    @Path(REST_RESOURCE)
    public static class TestResource {

        private String m_response;

        public TestResource() {
            m_response = DEFAULT_RESPONSE;
        }

        public TestResource(String response) {
            m_response = response;
        }

        @GET
        @Produces(MediaType.TEXT_PLAIN)
        public String handleGetRequest() {
            return m_response;
        }
    }

    public static class TestServletContextHelper extends ServletContextHelper {

    }

    public void testMultipleContexts() throws Exception {
        registerService(ServletContextHelper.class, new TestServletContextHelper(), HTTP_WHITEBOARD_CONTEXT_NAME,
            CONTEXT_1_NAME, HTTP_WHITEBOARD_CONTEXT_PATH, CONTEXT_1_PREFIX, AMDATU_WEB_WINK_REST_PATH, "");

        ServiceRegistration<ServletContextHelper> ctx2Reg =
            registerService(ServletContextHelper.class, new TestServletContextHelper(), HTTP_WHITEBOARD_CONTEXT_NAME,
                CONTEXT_2_NAME, HTTP_WHITEBOARD_CONTEXT_PATH, CONTEXT_2_PREFIX, AMDATU_WEB_WINK_REST_PATH, "");

        registerService(Object.class, new TestResource());

        registerService(Object.class, new TestResource(CONTEXT_1_NAME), HTTP_WHITEBOARD_CONTEXT_NAME, CONTEXT_1_NAME);

        registerService(Object.class, new TestResource(CONTEXT_2_NAME), HTTP_WHITEBOARD_CONTEXT_NAME, CONTEXT_2_NAME);

        assertResponseContent(DEFAULT_RESPONSE, REST_RESOURCE);
        assertResponseContent(CONTEXT_1_NAME, CONTEXT_1_PREFIX + "/" + REST_RESOURCE);
        assertResponseContent(CONTEXT_2_NAME, CONTEXT_2_PREFIX + "/" + REST_RESOURCE);

        // Expect the resource to become unavailable when the context is removed
        ctx2Reg.unregister();
        assertResponseContent(DEFAULT_RESPONSE, REST_RESOURCE);
        assertResponseContent(CONTEXT_1_NAME, CONTEXT_1_PREFIX + "/" + REST_RESOURCE);
        assertResponseCode(HTTP_NOT_FOUND, CONTEXT_2_PREFIX + "/" + REST_RESOURCE);

    }

    public void testContextResourcesAvailableBeforeContext() throws Exception {
        registerService(Object.class, new TestResource());

        registerService(Object.class, new TestResource(CONTEXT_1_NAME), HTTP_WHITEBOARD_CONTEXT_NAME, CONTEXT_1_NAME);

        assertResponseContent(DEFAULT_RESPONSE, REST_RESOURCE);
        assertResponseCode(HTTP_NOT_FOUND, CONTEXT_1_PREFIX + "/" + REST_RESOURCE);

        registerService(ServletContextHelper.class, new TestServletContextHelper(), HTTP_WHITEBOARD_CONTEXT_NAME,
            CONTEXT_1_NAME, HTTP_WHITEBOARD_CONTEXT_PATH, CONTEXT_1_PREFIX, AMDATU_WEB_WINK_REST_PATH, "");

        // Expect the resource to become available when the context is registered
        assertResponseContent(DEFAULT_RESPONSE, REST_RESOURCE);
        assertResponseContent(CONTEXT_1_NAME, CONTEXT_1_PREFIX + "/" + REST_RESOURCE);
    }

    public void testContextPath() throws Exception {
        ServiceRegistration<ServletContextHelper> ctxReg =
            registerService(ServletContextHelper.class, new TestServletContextHelper(), HTTP_WHITEBOARD_CONTEXT_NAME,
                CONTEXT_1_NAME, HTTP_WHITEBOARD_CONTEXT_PATH, CONTEXT_1_PREFIX, AMDATU_WEB_WINK_REST_PATH, "/rest");

        ServiceRegistration<Object> restContextSrvReg =
            registerService(Object.class, new TestResource(), HTTP_WHITEBOARD_CONTEXT_NAME, CONTEXT_1_NAME);

        assertResponseCode(HTTP_OK, CONTEXT_1_PREFIX + "/rest/" + REST_RESOURCE);

        ctxReg.unregister();
        restContextSrvReg.unregister();
    }

    /**
     * Make sure updating the wink.rest.path in configadmin does work for the default context but doesn't affect other contexts
     */
    public void testDefaultContextUpdateDoesntAffectOtherContext() throws Exception {
        registerService(ServletContextHelper.class, new TestServletContextHelper(), HTTP_WHITEBOARD_CONTEXT_NAME,
            CONTEXT_1_NAME, HTTP_WHITEBOARD_CONTEXT_PATH, CONTEXT_1_PREFIX, AMDATU_WEB_WINK_REST_PATH, "");

        registerService(Object.class, new TestResource());

        registerService(Object.class, new TestResource(CONTEXT_1_NAME), HTTP_WHITEBOARD_CONTEXT_NAME, CONTEXT_1_NAME);

        assertResponseContent(DEFAULT_RESPONSE, REST_RESOURCE);
        assertResponseContent(CONTEXT_1_NAME, CONTEXT_1_PREFIX + "/" + REST_RESOURCE);

        ManagedService service = getWinkManagedService();
        service.updated(asDictionary("wink.rest.path", PREFIX));

        assertResponseContent(DEFAULT_RESPONSE, PREFIX + "/" + REST_RESOURCE);
        assertResponseContent(CONTEXT_1_NAME, CONTEXT_1_PREFIX + "/" + REST_RESOURCE);

        service.updated(asDictionary("wink.rest.path", ""));

        assertResponseContent(DEFAULT_RESPONSE, REST_RESOURCE);
        assertResponseContent(CONTEXT_1_NAME, CONTEXT_1_PREFIX + "/" + REST_RESOURCE);
    }

    public void testDisableDefailtContext() throws Exception {
        registerService(Object.class, new TestResource());

        registerService(ServletContextHelper.class, new TestServletContextHelper(), HTTP_WHITEBOARD_CONTEXT_NAME,
            CONTEXT_1_NAME, HTTP_WHITEBOARD_CONTEXT_PATH, CONTEXT_1_PREFIX, AMDATU_WEB_WINK_REST_PATH, "");

        registerService(Object.class, new TestResource(CONTEXT_1_NAME), HTTP_WHITEBOARD_CONTEXT_NAME, CONTEXT_1_NAME);

        ManagedService service = getWinkManagedService();

        assertResponseCode(HTTP_OK, REST_RESOURCE);
        assertResponseCode(HTTP_OK, CONTEXT_1_PREFIX + "/" + REST_RESOURCE);

        service.updated(asDictionary("default.context.disabled", "true"));

        assertResponseCode(HTTP_NOT_FOUND, REST_RESOURCE);
        assertResponseCode(HTTP_OK, CONTEXT_1_PREFIX + "/" + REST_RESOURCE);

        service.updated(asDictionary("default.context.disabled", "false"));

        assertResponseCode(HTTP_OK, REST_RESOURCE);
        assertResponseCode(HTTP_OK, CONTEXT_1_PREFIX + "/" + REST_RESOURCE);
    }

    public void testChangeContextServiceRegistrationProps() throws Exception {
        registerService(Object.class, new TestResource());

        ServiceRegistration<ServletContextHelper> registration =
            registerService(ServletContextHelper.class, new TestServletContextHelper(), HTTP_WHITEBOARD_CONTEXT_NAME,
                CONTEXT_1_NAME, HTTP_WHITEBOARD_CONTEXT_PATH, CONTEXT_1_PREFIX);

        registerService(Object.class, new TestResource(CONTEXT_1_NAME), HTTP_WHITEBOARD_CONTEXT_NAME, CONTEXT_1_NAME);

        assertResponseCode(HTTP_OK, REST_RESOURCE);
        assertResponseCode(HTTP_NOT_FOUND, CONTEXT_1_PREFIX + "/" + REST_RESOURCE);
        assertResponseCode(HTTP_NOT_FOUND, CONTEXT_2_PREFIX + "/" + REST_RESOURCE);

        registration.setProperties(asDictionary(HTTP_WHITEBOARD_CONTEXT_NAME, CONTEXT_1_NAME,
            HTTP_WHITEBOARD_CONTEXT_PATH, CONTEXT_1_PREFIX, AMDATU_WEB_WINK_REST_PATH, ""));

        assertResponseCode(HTTP_OK, REST_RESOURCE);
        assertResponseCode(HTTP_OK, CONTEXT_1_PREFIX + "/" + REST_RESOURCE);
        assertResponseCode(HTTP_NOT_FOUND, CONTEXT_2_PREFIX + "/" + REST_RESOURCE);

        registration.setProperties(asDictionary(HTTP_WHITEBOARD_CONTEXT_NAME, CONTEXT_1_NAME,
            HTTP_WHITEBOARD_CONTEXT_PATH, CONTEXT_2_PREFIX, AMDATU_WEB_WINK_REST_PATH, ""));

        assertResponseCode(HTTP_OK, REST_RESOURCE);
        assertResponseCode(HTTP_NOT_FOUND, CONTEXT_1_PREFIX + "/" + REST_RESOURCE);
        assertResponseCode(HTTP_OK, CONTEXT_2_PREFIX + "/" + REST_RESOURCE);
    }
    
}

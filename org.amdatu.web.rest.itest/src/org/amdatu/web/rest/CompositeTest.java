/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest;

import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_FILTER_PATTERN;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_SERVLET_PATTERN;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.ManagedService;

/**
 * Tests the (de-)registration of resources
 */
public class CompositeTest extends RegistrationTestBase {

    public static class TestFilter implements Filter {
        @Override
        public void destroy() {
            // Nop
        }

        @Override
        public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
            try (PrintWriter writer = resp.getWriter()) {
                writer.print("filter");
            }
        }

        @Override
        public void init(FilterConfig config) throws ServletException {
            // Nop
        }
    }

    @Path(REST_RESOURCE)
    public static class TestResource {
        @GET
        @Produces(MediaType.TEXT_PLAIN)
        public String handleGetRequest() {
            return "rest";
        }
    }

    public static class TestServlet extends HttpServlet {
        private static final long serialVersionUID = 1L;

        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            try (PrintWriter writer = resp.getWriter()) {
                writer.println("servlet");
            }
        }
    }

    private static final String PREFIX = "/prefix";
    private static final String REST_RESOURCE = "/rest";
    private static final String FILTER_PATH = "/filter";
    private static final String SERVLET_PATH = "/servlet";

    /**
     * Tests that we can configure Wink itself by providing it the correct configuration options.
     */
    public void testConfigureWinkInternalsOk() throws Exception {
        registerService(Object.class, new TestResource());

        URL url = toURL(REST_RESOURCE);

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        try {
            String contentType = conn.getHeaderField("Content-Type");
            assertEquals("text/plain", contentType);
        } finally {
            conn.disconnect();
        }

        ManagedService service = getWinkManagedService();
        service.updated(asDictionary("wink.response.defaultCharset", "true"));

        conn = (HttpURLConnection) url.openConnection();
        try {
            String contentType = conn.getHeaderField("Content-Type");
            assertEquals("text/plain;charset=UTF-8", contentType);
        } finally {
            conn.disconnect();
        }
    }

    /**
     * TODO: Have another look at this test now were using a filter again
     * 
     * Tests that our WinkFilter does not get in the way of other registered {@link Filter}s and/or {@link Servlet}s.
     */
    public void testResourceRegistrationWithNonResourceFilter() throws Exception {
        ServiceRegistration<Object> srvReg = registerService(Object.class, new TestResource());
        ServiceRegistration<Filter> filterReg = registerService(Filter.class, new TestFilter(), HTTP_WHITEBOARD_FILTER_PATTERN, FILTER_PATH);
        ServiceRegistration<Servlet> servletReg = registerService(Servlet.class, new TestServlet(), HTTP_WHITEBOARD_SERVLET_PATTERN, SERVLET_PATH);

        assertResponseCode(HTTP_OK, REST_RESOURCE);
        assertResponseCode(HTTP_OK, FILTER_PATH);
        assertResponseCode(HTTP_OK, SERVLET_PATH);

        srvReg.unregister();

        assertResponseCode(HTTP_NOT_FOUND, REST_RESOURCE);
        assertResponseCode(HTTP_OK, FILTER_PATH);
        assertResponseCode(HTTP_OK, SERVLET_PATH);

        srvReg = registerService(Object.class, new TestResource());

        ManagedService service = getWinkManagedService();
        service.updated(asDictionary("wink.rest.path", PREFIX));

        assertResponseCode(HTTP_NOT_FOUND, REST_RESOURCE);
        assertResponseCode(HTTP_OK, PREFIX.concat(REST_RESOURCE));
//        assertResponseCode(HTTP_OK, FILTER_PATH); // TODO returns 404 now
        assertResponseCode(HTTP_OK, SERVLET_PATH);

        filterReg.unregister();

        assertResponseCode(HTTP_OK, PREFIX.concat(REST_RESOURCE));
        assertResponseCode(HTTP_NOT_FOUND, FILTER_PATH);
        assertResponseCode(HTTP_OK, SERVLET_PATH);

        servletReg.unregister();

        assertResponseCode(HTTP_OK, PREFIX.concat(REST_RESOURCE));
        assertResponseCode(HTTP_NOT_FOUND, FILTER_PATH);
        assertResponseCode(HTTP_NOT_FOUND, SERVLET_PATH);

        srvReg.unregister();

        assertResponseCode(HTTP_NOT_FOUND, REST_RESOURCE);
        assertResponseCode(HTTP_NOT_FOUND, FILTER_PATH);
        assertResponseCode(HTTP_NOT_FOUND, SERVLET_PATH);
    }

}

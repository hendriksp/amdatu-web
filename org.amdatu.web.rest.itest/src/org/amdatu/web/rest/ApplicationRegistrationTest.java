/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest;

import java.util.Collections;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.osgi.framework.Constants;
import org.osgi.framework.ServiceRegistration;

/**
 * Tests the (de-)registration of applications
 */
public class ApplicationRegistrationTest extends RegistrationTestBase {

    public static class TestApplication extends Application {
        private Set<Object> m_singletons;
        
        public TestApplication(int mappedCode) {
            m_singletons = Collections.singleton(new TestExceptionMapper(mappedCode));
        }
        
        @Override
        public Set<Object> getSingletons() {
            return m_singletons;
        }
    }

    @Provider
    public static class TestExceptionMapper implements ExceptionMapper<RuntimeException> {
        private int status;

        public TestExceptionMapper(int status) {
            this.status = status;
        }

        @Override
        public Response toResponse(RuntimeException arg0) {
            return Response.status(status).build();
        }
    }

    @Path("test")
    public static class TestResource {
        @GET
        public String handleGetRequest() {
            throw new RuntimeException("TEST EXCEPTION, IGNORE!");
        }
    }

    public void testTestRegistration() throws Exception {
        String url = "/test";

        ServiceRegistration<Object> resRegistration = registerService(Object.class, new TestResource());

        assertResponseCode(HTTP_INTERNAL_SERVER_ERROR, url);

        ServiceRegistration<Object> app1Registration = registerService(Object.class, new TestApplication(HTTP_OK), Constants.SERVICE_RANKING, 1);

        assertResponseCode(HTTP_OK, url);

        // Higher ranking means that it is chosen first...
        ServiceRegistration<Object> app2Registration = registerService(Object.class, new TestApplication(HTTP_FORBIDDEN), Constants.SERVICE_RANKING, 200);

        assertResponseCode(HTTP_FORBIDDEN, url);

        app2Registration.unregister();

        assertResponseCode(HTTP_OK, url);

        app1Registration.unregister();

        assertResponseCode(HTTP_INTERNAL_SERVER_ERROR, url);

        resRegistration.unregister();
    }
}

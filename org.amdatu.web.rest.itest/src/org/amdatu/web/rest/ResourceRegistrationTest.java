/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest;

import java.util.Dictionary;
import java.util.Hashtable;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.ManagedService;

/**
 * Tests the (de-)registration of resources
 */
public class ResourceRegistrationTest extends RegistrationTestBase {
    private static final String PREFIX = "/prefix";
    private static final String BASE = "/base";
    private static final String SUB = "/base/sub";

    @Path(BASE)
    public static class TestBaseResource {
        @GET
        public String handleGetRequest() {
            return "base";
        }
    }

    @Path(SUB)
    public static class TestSubResource {
        @GET
        public String handleGetRequest() {
            return "sub";
        }
    }

    public void testBaseResourceRegistration() throws Exception {
        assertResponseCode(HTTP_NOT_FOUND, BASE);

        ServiceRegistration<Object> srvReg = registerService(Object.class, new TestBaseResource());

        assertResponseCode(HTTP_OK, BASE);

        srvReg.unregister();

        assertResponseCode(HTTP_NOT_FOUND, BASE);
    }

    public void testBaseResourceRegistrationWithPrefixOverride() throws Exception {
        String prefixedBase = PREFIX.concat(BASE);

        assertResponseCode(HTTP_NOT_FOUND, prefixedBase);
        assertResponseCode(HTTP_NOT_FOUND, BASE);

        ServiceRegistration<Object> baseSrvReg = registerService(Object.class, new TestBaseResource());

        assertResponseCode(HTTP_OK, BASE);

        Dictionary<String, Object> properties = new Hashtable<>();
        properties.put("wink.rest.path", PREFIX);

        ManagedService service = getWinkManagedService();
        service.updated(properties);

        assertResponseCode(HTTP_NOT_FOUND, BASE);
        assertResponseCode(HTTP_OK, prefixedBase);

        service.updated(null);

        assertResponseCode(HTTP_NOT_FOUND, prefixedBase);
        assertResponseCode(HTTP_OK, BASE);

        baseSrvReg.unregister();

        assertResponseCode(HTTP_NOT_FOUND, prefixedBase);
        assertResponseCode(HTTP_NOT_FOUND, BASE);
    }

    /**
     * Tests that we can "extend" a resource by using the same base path an existing resource.
     */
    public void testBaseWithSubResourceRegistration() throws Exception {
        assertResponseCode(HTTP_NOT_FOUND, BASE);
        assertResponseCode(HTTP_NOT_FOUND, SUB);

        ServiceRegistration<Object> baseSrvReg = registerService(Object.class, new TestBaseResource());
        ServiceRegistration<Object> subSrvReg = registerService(Object.class, new TestSubResource());

        assertResponseCode(HTTP_OK, BASE);
        assertResponseCode(HTTP_OK, SUB);

        baseSrvReg.unregister();

        assertResponseCode(HTTP_NOT_FOUND, BASE);
        assertResponseCode(HTTP_OK, SUB);

        subSrvReg.unregister();

        assertResponseCode(HTTP_NOT_FOUND, BASE);
        assertResponseCode(HTTP_NOT_FOUND, SUB);
    }

    public void testBaseWithSubResourceRegistrationWithPrefixOverride() throws Exception {
        String prefixedBase = PREFIX.concat(BASE);
        String prefixedSub = PREFIX.concat(SUB);

        assertResponseCode(HTTP_NOT_FOUND, prefixedBase);
        assertResponseCode(HTTP_NOT_FOUND, BASE);

        ServiceRegistration<Object> baseSrvReg = registerService(Object.class, new TestBaseResource());
        ServiceRegistration<Object> subSrvReg = registerService(Object.class, new TestSubResource());

        assertResponseCode(HTTP_OK, BASE);
        assertResponseCode(HTTP_OK, SUB);

        Dictionary<String, Object> properties = new Hashtable<>();
        properties.put("wink.rest.path", PREFIX);

        ManagedService service = getWinkManagedService();
        service.updated(properties);

        assertResponseCode(HTTP_OK, prefixedBase);
        assertResponseCode(HTTP_OK, prefixedSub);

        service.updated(null);

        assertResponseCode(HTTP_NOT_FOUND, prefixedBase);
        assertResponseCode(HTTP_OK, BASE);
        assertResponseCode(HTTP_OK, SUB);

        baseSrvReg.unregister();

        assertResponseCode(HTTP_NOT_FOUND, prefixedBase);
        assertResponseCode(HTTP_NOT_FOUND, BASE);
        assertResponseCode(HTTP_OK, SUB);
        
        subSrvReg.unregister();

        assertResponseCode(HTTP_NOT_FOUND, prefixedBase);
        assertResponseCode(HTTP_NOT_FOUND, BASE);
        assertResponseCode(HTTP_NOT_FOUND, SUB);
    }
}

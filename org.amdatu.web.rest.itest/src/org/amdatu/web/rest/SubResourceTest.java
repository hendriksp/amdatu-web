/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.osgi.framework.ServiceRegistration;

public class SubResourceTest extends RegistrationTestBase {
    
    @Path("root")
    public static class TestResource {
        @GET
        @Produces(MediaType.TEXT_PLAIN)
        public String handleGetRequest() {
            return "root";
        }
    }
    
    @Path("root")
    public static class TestResourceSubA {
        @GET
        @Path("a")
        @Produces(MediaType.TEXT_PLAIN)
        public String handleGetRequest() {
            return "sub-a";
        }
    }
    
    @Path("root")
    public static class TestResourceSubB {
        @GET
        @Path("b")
        @Produces(MediaType.TEXT_PLAIN)
        public String handleGetRequest() {
            return "sub-b";
        }
    }
    
    public void testSubResources() throws Exception {
        ServiceRegistration<Object> rootReg = registerService(Object.class, new TestResource());
        ServiceRegistration<Object> subAReg = registerService(Object.class, new TestResourceSubA());
        ServiceRegistration<Object> subBReg = registerService(Object.class, new TestResourceSubB());
        
        
        assertResponseContent("root", "root");
        assertResponseContent("sub-a", "root/a");
        assertResponseContent("sub-b", "root/b");
        
        rootReg.unregister();
        assertResponseCode(405, "root");
        assertResponseContent("sub-a", "root/a");
        assertResponseContent("sub-b", "root/b");
        
        subAReg.unregister();
        /*
         * root will return a 405 response as there is a resource but 
         * there is no method GET method on "root/"
         */
        assertResponseCode(405, "root");  
        assertResponseCode(HTTP_NOT_FOUND, "root/a");
        assertResponseContent("sub-b", "root/b");
        
        subBReg.unregister();
        Thread.sleep(100l);
        assertResponseCode(HTTP_NOT_FOUND, "root");
        assertResponseCode(HTTP_NOT_FOUND, "root/a");
        assertResponseCode(HTTP_NOT_FOUND, "root/b");
    }

    
}

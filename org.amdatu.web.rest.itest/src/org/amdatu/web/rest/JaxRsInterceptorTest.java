/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest;

import static org.amdatu.web.rest.jaxrs.JaxRsRequestInterceptor.REQUEST_INTERCEPTOR_ID;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.WebApplicationException;

import org.amdatu.web.rest.jaxrs.InterceptRequest;
import org.amdatu.web.rest.jaxrs.JaxRsRequestInterceptor;
import org.amdatu.web.rest.jaxrs.RequestContext;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceRegistration;

/**
 * Tests the usage of {@link JaxRsRequestInterceptor}
 */
public class JaxRsInterceptorTest extends RegistrationTestBase {

    private static final String RESOURCE = "/test";

    @Retention(RetentionPolicy.RUNTIME)
    @Target( { ElementType.TYPE, ElementType.METHOD })
    @InterceptRequest(id = "custom")
    public static @interface CustomInterceptor { }
    
    @Path(RESOURCE)
    @InterceptRequest(id = "test")
    public static class TestAnnotatedResource {
        
        @GET
        public String handleGetRequest() {
            return "test";
        }
    }
    
    @Path(RESOURCE)
    public static class TestResourceWithAnnotatedMethod {

        @GET
        @InterceptRequest(id = "test")
        public String handleGetRequest() {
            return "test";
        }
    }
    
    @Path(RESOURCE)
    @InterceptRequest(id = "test")
    public static class TestAnnotatedResourceWithAnnotatedMethod {

        @GET
        @InterceptRequest(id = "test2")
        public String handleGetRequest() {
            return "test";
        }
    }
    
    @Path(RESOURCE)
    @CustomInterceptor
    public static class TestCustomInterceptorAnnotatedResource {
        
        @GET
        public String handleGetRequest() {
            return "test";
        }
    }
    
    public static class TestInterceptor implements JaxRsRequestInterceptor {
        
        private final Integer m_status;
        
        public TestInterceptor() {
            m_status = null;
        }
        
        public TestInterceptor(int status) {
            m_status = status;
        }
        
        @Override
        public void intercept(RequestContext context) {
            if (m_status != null) {
                throw new WebApplicationException(m_status);                
            }
        }
                
    } 
    
    public void testInterceptorAnnotatedResource() throws Exception {
        assertResponseCode(HTTP_NOT_FOUND, RESOURCE);

        registerService(Object.class, new TestResourceWithAnnotatedMethod());
        assertResponseCode(HTTP_INTERNAL_SERVER_ERROR, RESOURCE);
        
        ServiceRegistration<JaxRsRequestInterceptor> interceptor1 = registerService(JaxRsRequestInterceptor.class,
            new TestInterceptor(400), REQUEST_INTERCEPTOR_ID, "test", Constants.SERVICE_RANKING, 1);

        assertResponseCode(400, RESOURCE);

        ServiceRegistration<JaxRsRequestInterceptor> interceptor2 = registerService(JaxRsRequestInterceptor.class,
            new TestInterceptor(HTTP_FORBIDDEN), REQUEST_INTERCEPTOR_ID, "test", Constants.SERVICE_RANKING, 2);
        
        assertResponseCode(HTTP_FORBIDDEN, RESOURCE);
        
        interceptor2.unregister();
        assertResponseCode(400, RESOURCE);
        
        interceptor1.unregister();
        assertResponseCode(HTTP_INTERNAL_SERVER_ERROR, RESOURCE);
    }
    
    public void testInterceptorAnnotatedMethod() throws Exception {
        assertResponseCode(HTTP_NOT_FOUND, RESOURCE);

        registerService(Object.class, new TestResourceWithAnnotatedMethod());
        assertResponseCode(HTTP_INTERNAL_SERVER_ERROR, RESOURCE);
        
        ServiceRegistration<JaxRsRequestInterceptor> interceptor1 = registerService(JaxRsRequestInterceptor.class,
            new TestInterceptor(400), REQUEST_INTERCEPTOR_ID, "test", Constants.SERVICE_RANKING, 1);

        assertResponseCode(400, RESOURCE);

        ServiceRegistration<JaxRsRequestInterceptor> interceptor2 = registerService(JaxRsRequestInterceptor.class,
            new TestInterceptor(HTTP_FORBIDDEN), REQUEST_INTERCEPTOR_ID, "test", Constants.SERVICE_RANKING, 2);
        
        assertResponseCode(HTTP_FORBIDDEN, RESOURCE);
        
        interceptor2.unregister();
        assertResponseCode(400, RESOURCE);
        
        interceptor1.unregister();
        assertResponseCode(HTTP_INTERNAL_SERVER_ERROR, RESOURCE);
    }
    
    public void testInterceptorAnnotatedResourceAndMethod() throws Exception {
        assertResponseCode(HTTP_NOT_FOUND, RESOURCE);

        registerService(Object.class, new TestAnnotatedResourceWithAnnotatedMethod());
        assertResponseCode(HTTP_INTERNAL_SERVER_ERROR, RESOURCE);
        
        ServiceRegistration<JaxRsRequestInterceptor> interceptor1 = registerService(JaxRsRequestInterceptor.class,
            new TestInterceptor(), REQUEST_INTERCEPTOR_ID, "test", Constants.SERVICE_RANKING, 1);

        assertResponseCode(HTTP_INTERNAL_SERVER_ERROR, RESOURCE);
        
        ServiceRegistration<JaxRsRequestInterceptor> interceptor2 = registerService(JaxRsRequestInterceptor.class,
            new TestInterceptor(HTTP_FORBIDDEN), REQUEST_INTERCEPTOR_ID, "test2", Constants.SERVICE_RANKING, 2);

        assertResponseCode(HTTP_FORBIDDEN, RESOURCE);
        
        interceptor2.unregister();
        assertResponseCode(HTTP_INTERNAL_SERVER_ERROR, RESOURCE);
        
        interceptor1.unregister();
        assertResponseCode(HTTP_INTERNAL_SERVER_ERROR, RESOURCE);
    }
    
    public void testCustomInterceptorAnnotatedResource() throws Exception {
        assertResponseCode(HTTP_NOT_FOUND, RESOURCE);

        registerService(Object.class, new TestCustomInterceptorAnnotatedResource());
        assertResponseCode(HTTP_INTERNAL_SERVER_ERROR, RESOURCE);
        
        ServiceRegistration<JaxRsRequestInterceptor> interceptor = registerService(JaxRsRequestInterceptor.class,
            new TestInterceptor(HTTP_FORBIDDEN), REQUEST_INTERCEPTOR_ID, "custom", Constants.SERVICE_RANKING, 1);

        assertResponseCode(HTTP_FORBIDDEN, RESOURCE);
        
        interceptor.unregister();
        assertResponseCode(HTTP_INTERNAL_SERVER_ERROR, RESOURCE);
    }

}

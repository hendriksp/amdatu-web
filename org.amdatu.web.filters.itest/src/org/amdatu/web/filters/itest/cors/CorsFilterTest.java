/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.filters.itest.cors;

import java.util.Collection;
import java.util.Dictionary;
import java.util.Hashtable;

import org.osgi.framework.Constants;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.ManagedService;

public class CorsFilterTest extends FilterTestBase {

    @Override
    protected void setUp() throws Exception {
//        Thread.sleep(500000l);
        super.setUp();
    }
    
    public void testDefaultHeaders() throws Exception {
        assertDefaults(URL_HELLO_WORLD);
    }

    private void assertDefaults(String url) throws Exception {
        assertEquals(200, getResponseCode(url));
        assertEquals("true", 
            getHeader(url, "Access-Control-Allow-Credentials"));
        assertEquals("Origin, Content-Type, If-Match", 
            getHeader(url, "Access-Control-Allow-Headers"));
        assertEquals("GET, POST, PUT, DELETE, OPTIONS, HEAD", 
            getHeader(url, "Access-Control-Allow-Methods"));
        assertEquals("*", 
            getHeader(url, "Access-Control-Allow-Origin"));
        assertEquals("Link, Location, ETag", 
            getHeader(url, "Access-Control-Expose-Headers"));
    }
    
    public void testCustomHeaders() throws Exception { 
        
        ManagedService service = getService();
        Dictionary<String, Object> props = new Hashtable<>();
        
        props.put("allowCredentials", "allowCredentials");;
        props.put("allowHeaders", "allowHeaders");
        props.put("allowMethods", "allowMethods");
        props.put("allowOrigin", "allowOrigin");
        props.put("exposeHeaders", "exposeHeaders");
        
        service.updated(props);
        assertEquals("allowCredentials", 
            getHeader(URL_HELLO_WORLD, "Access-Control-Allow-Credentials"));
        assertEquals("allowHeaders", 
            getHeader(URL_HELLO_WORLD, "Access-Control-Allow-Headers"));
        assertEquals("allowMethods", 
            getHeader(URL_HELLO_WORLD, "Access-Control-Allow-Methods"));
        assertEquals("allowOrigin", 
            getHeader(URL_HELLO_WORLD, "Access-Control-Allow-Origin"));
        assertEquals("exposeHeaders", 
            getHeader(URL_HELLO_WORLD, "Access-Control-Expose-Headers"));
        
        service.updated(null);
        assertDefaults(URL_HELLO_WORLD);
        
    }

    public void testPattern() throws Exception {
        ManagedService service = getService();
        Dictionary<String, Object> props = new Hashtable<>();
        props.put("pattern", "^/hello.*");
        service.updated(props);
        
        assertDefaults(URL_HELLO_WORLD);
        assertNull(getHeader(URL_FAREWELL_WORLD, "Access-Control-Allow-Credentials"));
        assertNull(getHeader(URL_FAREWELL_WORLD, "Access-Control-Allow-Headers"));
        assertNull(getHeader(URL_FAREWELL_WORLD, "Access-Control-Allow-Methods"));
        assertNull(getHeader(URL_FAREWELL_WORLD, "Access-Control-Allow-Origin"));
        assertNull(getHeader(URL_FAREWELL_WORLD, "Access-Control-Expose-Headers"));
        
        props.put("pattern", "^/farewell.*");
        service.updated(props);
        
        assertNull(getHeader(URL_HELLO_WORLD, "Access-Control-Allow-Credentials"));
        assertNull(getHeader(URL_HELLO_WORLD, "Access-Control-Allow-Headers"));
        assertNull(getHeader(URL_HELLO_WORLD, "Access-Control-Allow-Methods"));
        assertNull(getHeader(URL_HELLO_WORLD, "Access-Control-Allow-Origin"));
        assertNull(getHeader(URL_HELLO_WORLD, "Access-Control-Expose-Headers"));
        
        assertDefaults(URL_FAREWELL_WORLD);
        
        service.updated(null);
        assertDefaults(URL_HELLO_WORLD);
        assertDefaults(URL_FAREWELL_WORLD);
    }
    
    private ManagedService getService() {
        try {
            Collection<ServiceReference<ManagedService>> references;
            references = m_context.getServiceReferences(ManagedService.class, 
                                String.format("(%s=%s)", Constants.SERVICE_PID, "org.amdatu.web.filters.cors"));
            if (references.size() != 1) {
                fail();
            }
            ManagedService service = (ManagedService) m_context.getService(references.iterator().next());
            return service;
        } catch (InvalidSyntaxException e) {
            throw new RuntimeException("should never happen");
        }
    }
    
}

/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.filters.itest.cors;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;

import junit.framework.TestCase;

public abstract class FilterTestBase extends TestCase {

    protected static final int HTTP_OK = 200;
    protected static final int HTTP_NOT_FOUND = 404;
    private static final String PREFIX = "http://localhost:8080";
    private static final String SLASH = "/";
    protected static final String URL_HELLO_WORLD = "hello/helloworld.txt";
    protected static final String URL_FAREWELL_WORLD = "farewell/farewellworld.txt";
    
    protected final BundleContext m_context;
    
    protected FilterTestBase() {
        m_context = FrameworkUtil.getBundle(getClass()).getBundleContext();
    }
    
    /**
     * @return the HTTP header field, can be <code>null</code>.
     */
    protected String getHeader(String urlToRead, String headerName) throws Exception {
        List<String> values = getHeaderValues(urlToRead, headerName);
        return values.isEmpty() ? null : values.get(0);
    }

    /**
     * @return the HTTP header field, can be <code>null</code>.
     */
    private Map<String, List<String>> getHeaders(String urlToRead) throws Exception {
        URL url = toURL(urlToRead);

        HttpURLConnection conn = null;

        try {
            conn = (HttpURLConnection) url.openConnection();

            int rc = conn.getResponseCode();
            assertEquals(HTTP_OK, rc);

            return conn.getHeaderFields();
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
    }

    /**
     * @return the values of the HTTP header field, never <code>null</code>.
     */
    protected List<String> getHeaderValues(String urlToRead, String headerName) throws Exception {
        Map<String, List<String>> headers = getHeaders(urlToRead);
        List<String> result = headers.get(headerName);
        return result == null ? Collections.<String> emptyList() : result;
    }

    /**
     * @return the HTTP response code for the given URL, e.g., 200 or 404.
     */
    protected int getResponseCode(String urlToRead) throws Exception {
        URL url = toURL(urlToRead);

        HttpURLConnection conn = null;
        try {
            conn = (HttpURLConnection) url.openConnection();
            int rc = conn.getResponseCode();
            return rc;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
    }

    private URL toURL(String url) throws MalformedURLException {
        if (!url.startsWith(PREFIX)) {
            if (!url.startsWith(SLASH)) {
                url = PREFIX + SLASH + url;
            } else {
                url = PREFIX + url;
            }
        }
        return new URL(url);
    }
    
}

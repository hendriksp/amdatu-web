/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.concurrency.itest.conf;

import org.amdatu.web.rest.concurrency.itest.configuration.api.ConfigurationService;

public class ExampleConfigurationService {
    private volatile ConfigurationService m_configuration;

    public void start() {
        // Start with a clean slate
        m_configuration.reset();

        m_configuration.configure("org.amdatu.web.rest.wink", "wink.rest.path", "/rest");
    }

    public void stop() {
        m_configuration.reset();
    }
}

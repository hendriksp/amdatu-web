/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.concurrency.itest;

import static org.amdatu.web.rest.concurrency.itest.TestConstants.KEY_REST_PATH;
import static org.amdatu.web.rest.concurrency.itest.TestConstants.ORG_AMDATU_WINK_PID;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import javax.servlet.Servlet;

import junit.framework.TestCase;

import org.apache.felix.dm.Component;
import org.apache.felix.dm.ComponentState;
import org.apache.felix.dm.ComponentStateListener;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;

public class ConcurrentUpdateAndRegistrationTest extends TestCase {
    static enum Availability {
        PRESENT, NOT_PRESENT;
    }
    
    private static final Class<?>[] COMPONENTS = new Class[] { ComponentA.class, ComponentB.class, ComponentC.class, ComponentD.class, ComponentE.class, ComponentF.class };
    
    private static final int COMPONENT_COUNT = COMPONENTS.length;
    private static final int OTHER_SERVLET_COUNT = 11;

    private final BundleContext m_context = FrameworkUtil.getBundle(getClass()).getBundleContext();
    private final ExecutorService m_executor = Executors.newSingleThreadExecutor();

    private final List<Component> m_components = new ArrayList<Component>();
    
    protected final AtomicBoolean m_continueConfigure = new AtomicBoolean(true);
    protected final AtomicLong m_configChangeCount = new AtomicLong(0L);
    
    private Future<?> m_future;

    private volatile DependencyManager m_dm;
    private volatile ConfigurationAdmin m_configurationAdmin;

    /**
     * Overridden to ensure that our {@link #tearDown()} method is always called, even when {@link #setUp()} fails with
     * an exception (by default, JUnit does not call this method when the set up fails).
     *
     * @see junit.framework.TestCase#runBare()
     */
    @Override
    public final void runBare() throws Throwable {
        Throwable exception = null;
        try {
            setUp();

            runTest();
        } catch (Throwable running) {
            exception = running;
        } finally {
            try {
                tearDown();
            } catch (Throwable tearingDown) {
                if (exception == null)
                    exception = tearingDown;
            }
        }
        if (exception != null)
            throw exception;
    }
    
    public void testNop() {
        // Stub test to circumvent empty test cases...
    }

    /**
     * This test checks whether web application configuration updates through configuration admin can safely be executed when
     * servlet components are being registered in parallel. Tests AMDATUWEB-21.
     * <p>
     * AMDATUWEB-30 -&gt; no longer needed as we are no longer registering servlets per REST resource, so this issue should not manifest itself any longer.
     * </p>
     */
    public void disabledTestConcurrentConfigurationUpdateAndComponentRegistration() throws Exception {
        // baseline
        verifyServlets("/rest", Availability.NOT_PRESENT);

        System.out.println("Baseline verified...");

        // launch a configuration changing thread which just keeps on updating the configuration
        m_future = m_executor.submit(new ConfigurationChangeJob());

        // start registering our components
        registerComponents();

        // stop configuration change thread
        m_continueConfigure.set(false);

        assertNull(m_future.get(5, TimeUnit.SECONDS));
        assertTrue(m_future.isDone());

        // verify current situation
        verifyServlets(createPath(m_configChangeCount.get()), Availability.PRESENT);

        System.out.println("Components correctly registered...");

        // start configuration change thread
        m_continueConfigure.set(true);

        m_future = m_executor.submit(new ConfigurationChangeJob());

        // remove our components
        unregisterComponents();

        // stop configuration change thread
        m_continueConfigure.set(false);

        assertNull(m_future.get(5, TimeUnit.SECONDS));
        assertTrue(m_future.isDone());

        // verify current situation
        verifyServlets(createPath(m_configChangeCount.get()), Availability.NOT_PRESENT);

        System.out.println("Components correctly unregistered...");
    }

    @Override
    protected void setUp() throws Exception {
        final CountDownLatch latch = new CountDownLatch(1);

        m_dm = new DependencyManager(m_context);
        Component comp =
            m_dm.createComponent().setImplementation(this)
                .add(m_dm.createServiceDependency().setService(ConfigurationAdmin.class).setRequired(true));

        comp.add(new ComponentStateListener() {
			@Override
			public void changed(Component component, ComponentState state) {
				if (state == ComponentState.TRACKING_OPTIONAL) {
					latch.countDown();
				}
			}
        });
        m_dm.add(comp);

        assertTrue("Failed to obtain ConfigurationAdmin dependency?!", latch.await(10, TimeUnit.SECONDS));
    }

    @Override
    protected void tearDown() throws Exception {
        if (m_future != null) {
            m_future.cancel(true);

            assertNull(m_future.get(5, TimeUnit.SECONDS));
        }

        m_executor.shutdownNow();

        assertTrue(m_executor.awaitTermination(5, TimeUnit.SECONDS));
    }

    /**
     * Waits until {@link ConfigurationAdmin} has provisioned all configuration updates to Amdatu-Wink. This happens asynchronously, so
     * we cannot determine without polling the service properties when this is finished.
     */
    private void awaitConfigAdminResults(String expectedPath, Availability available) throws Exception {
        long start = System.currentTimeMillis();
        boolean loop = true;

        do {
            int seen = 0;
            for (ServiceReference<Servlet> ref : m_context.getServiceReferences(Servlet.class, null)) {
                String attr = (String) ref.getProperty("alias");
                if (attr != null && attr.startsWith(expectedPath)) {
                    seen++;
                }
            }
            if (available == Availability.PRESENT && seen == (COMPONENT_COUNT + OTHER_SERVLET_COUNT)) {
                loop = false;
            } else if (available == Availability.NOT_PRESENT && seen == OTHER_SERVLET_COUNT) {
                loop = false;
            } else {
                TimeUnit.MILLISECONDS.sleep(50);
            }
        } while (loop && ((System.currentTimeMillis() - start) < 60000));

        assertFalse("Failed to catch up with all configuration updates...", loop);
    }

    private void registerComponents() {
        for (Class<?> clazz : COMPONENTS) {
            Component component =
                m_dm.createComponent().setInterface(Object.class.getName(), null).setImplementation(clazz);
            m_components.add(component);
            m_dm.add(component);
        }
    }

    private void unregisterComponents() {
        List<Component> componentsToRemove = new ArrayList<Component>(m_components);
        for (Component component : componentsToRemove) {
            m_dm.remove(component);
            m_components.remove(component);
        }
    }

    private void verifyServlets(String prefix, Availability available) throws Exception {
        awaitConfigAdminResults(prefix, available);

        try {
            List<String> expectedAliases = new ArrayList<String>(Arrays.asList(prefix + "/invoke/A", prefix + "/invoke/B", prefix + "/invoke/C", 
                    prefix + "/invoke/D", prefix + "/invoke/E", prefix + "/invoke/F"));
            List<String> registeredAliases = new ArrayList<String>();
            for (ServiceReference<Servlet> ref : m_context.getServiceReferences(Servlet.class, null)) {
                final String alias = (String) ref.getProperty("alias");
                if (alias != null && alias.matches("^/\\d+/invoke/[A-F]$")) {
                    registeredAliases.add(alias);
                }
            }

            if (available == Availability.PRESENT) {
                // We should have seen all expected aliases...
                expectedAliases.removeAll(registeredAliases);
                assertTrue("Not all aliases were seen: " + expectedAliases + " (did see: " + registeredAliases + ")", expectedAliases.isEmpty());
            } else {
                // None of the expected aliases should be seen...
                assertTrue("Still some aliases were seen: " + registeredAliases, registeredAliases.isEmpty());
            }
        } catch (InvalidSyntaxException e) {
            fail("Null-filters should always be accepted?!");
        }
    }

    final class ConfigurationChangeJob implements Callable<Void> {
        /* Rate limit the number of changes as not to load ConfigAdmin with too much work... */
        static final int LIMIT = 50;
        
        @Override
        public Void call() throws Exception {
            int changes = 0;
            while (!Thread.currentThread().isInterrupted() && m_continueConfigure.get() && changes++ < LIMIT) {
                updateConfiguration(createPath(m_configChangeCount.incrementAndGet()));
            }
            return null;
        }

        private void updateConfiguration(String contextPath) {
            Dictionary<String, String> properties = new Hashtable<String, String>();
            properties.put(KEY_REST_PATH, contextPath);

            Configuration configuration;
            try {
                configuration = m_configurationAdmin.getConfiguration(ORG_AMDATU_WINK_PID, null);
                configuration.update(properties);
            } catch (IOException e) {
                fail("Error updating wink configuration. " + e.getMessage());
            }
        }
    }
    
    static String createPath(long value) {
        return String.format("/%04d", value);
    }
}

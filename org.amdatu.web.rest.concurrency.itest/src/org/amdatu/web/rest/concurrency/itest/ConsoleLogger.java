/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.concurrency.itest;

import org.osgi.framework.ServiceReference;
import org.osgi.service.log.LogService;

/**
 * An implementation of the OSGi LogService that directly outputs each log message
 * to <code>System.out</code>. It does not implement the LogReader or LogListeners.
 */
public class ConsoleLogger implements LogService {
    private static String[] LEVEL = { "", "ERROR", "WARN", "INFO", "DEBUG" };

    public void log(int level, String message) {
        log(null, level, message, null);
    }

    public void log(int level, String message, Throwable throwable) {
        log(null, level, message, throwable);
    }

    public void log(ServiceReference reference, int level, String message) {
        log(reference, level, message, null);
    }

    public void log(ServiceReference reference, int level, String message, Throwable throwable) {
        if (level == LogService.LOG_DEBUG) {
            return;
        }

        System.out.printf("[%-5s] %s%n", LEVEL[level], message);
        if (throwable != null) {
            throwable.printStackTrace(System.out);
        }
    }
}
